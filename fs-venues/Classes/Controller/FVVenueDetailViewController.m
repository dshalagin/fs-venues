//
//  FVVenueDetailViewController.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenueDetailViewController.h"
#import "FVNetworkImageView.h"
#import "FVVenue.h"

#define kVenueIconSize CGSizeMake(88.0f, 88.0f)

@implementation FVVenueDetailViewController {
    //view
    FVNetworkImageView *_iconView;
    UILabel *_titleLabel;
    UILabel *_categoryLabel;
    UILabel *_addressLabel;
    
    //data
    FVVenue *_venue;
}

#pragma mark - Initialization

- (id)initWithVenue:(FVVenue *)venue {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _venue = venue;
    }
    
    return self;
}

#pragma mark - View lifecycle

- (void)loadView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    _titleLabel.textColor = [FVAppStyle darkGrayColor];
    _titleLabel.numberOfLines = 0;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.text = _venue.name;
    [view addSubview:_titleLabel];
    
    _categoryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _categoryLabel.textColor = [FVAppStyle lightGrayColor];
    _categoryLabel.font = [UIFont systemFontOfSize:14.0f];
    _categoryLabel.numberOfLines = 0;
    _categoryLabel.text = [_venue primaryCategory].name;
    [view addSubview:_categoryLabel];
    
    _iconView = [[FVNetworkImageView alloc] initWithFrame:CGRectZero];
    [view addSubview:_iconView];
    
    _addressLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _addressLabel.font = [UIFont systemFontOfSize:14.0f];
    _addressLabel.textColor = [FVAppStyle grayColor];
    _addressLabel.numberOfLines = 0;
    _addressLabel.text = _venue.location.formattedAddress;
    [view addSubview:_addressLabel];
    
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Venue", nil);
    self.edgesForExtendedLayout = UIRectEdgeNone; //remove nav bar overlay
    
    _titleLabel.text = _venue.name;
    _categoryLabel.text = [_venue primaryCategory].name;
    _addressLabel.text = _venue.location.formattedAddress;
    [_iconView setImageWithURL:[NSURL URLWithString:[_venue.photo urlForSize:kVenueIconSize]]];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGFloat edgeOffset = 15.0f;
    CGFloat controlOffset = 10.0f;
    
    //layout venue icon
    _iconView.frame = CGRectMake(edgeOffset, edgeOffset, kVenueIconSize.width, kVenueIconSize.height);
    
    //calculate text content width
    CGFloat textWidth = self.view.width - _iconView.right - controlOffset - edgeOffset;
    
    //layout venue title
    _titleLabel.frame = CGRectMake(_iconView.right + controlOffset,
                                   _iconView.top,
                                   textWidth,
                                   0);
    [_titleLabel sizeToFit];
    
    //layout venue category
    _categoryLabel.frame = CGRectMake(_titleLabel.left,
                                      _titleLabel.bottom,
                                      textWidth,
                                      0);
    [_categoryLabel sizeToFit];
    
    //layout venue address
    _addressLabel.frame = CGRectMake(_titleLabel.left,
                                     _categoryLabel.bottom + controlOffset,
                                     textWidth,
                                     0);
    [_addressLabel sizeToFit];
}

@end
