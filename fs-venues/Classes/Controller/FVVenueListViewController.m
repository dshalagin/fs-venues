//
//  FVVenueListViewController.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenueListViewController.h"
#import "FVVenueDetailViewController.h"
#import "FVFoursquareModel.h"
#import "FVVenuesListView.h"
#import "FVVenuesMapView.h"

#import <CoreLocation/CoreLocation.h>

//------------------------------------------------------------
@interface FVVenueListViewController () <FVVenuesViewDelegate>
@end

//------------------------------------------------------------
@implementation FVVenueListViewController {
    UIView *_contentView;
    FVVenuesListView *_listView;
    FVVenuesMapView *_mapView;
    UIActivityIndicatorView *_activityIndicatorView;
    UIButton *_switchViewButton;
}

#pragma mark - View lifecycle

- (void)loadView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    _contentView = [[UIView alloc] initWithFrame:CGRectZero];
    _contentView.hidden = YES;
    [view addSubview:_contentView];
    
    _listView = [[FVVenuesListView alloc] initWithFrame:CGRectZero];
    _listView.delegate = self;
    [_contentView addSubview:_listView];
    
    _mapView = [[FVVenuesMapView alloc] initWithFrame:CGRectZero];
    _mapView.delegate = self;
    _mapView.hidden = YES;
    [_contentView addSubview:_mapView];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [view addSubview:_activityIndicatorView];
    
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Venues", nil);
    
    //create switch view nav bar button
    _switchViewButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    _switchViewButton.frame = CGRectMake(0.0f, 0.0f, 25.0f, 25.0f);
    [_switchViewButton addTarget:self action:@selector(actionSwitchView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_switchViewButton];
    
    //show loading indicator
    [_activityIndicatorView startAnimating];
    
    //show list by default
    [self showList];
    
    //location notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveUpdateLocationsNotification:)
                                                 name:FVLocationManagerDidUpdateLocationsNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveLocationManagerFailNotification:)
                                                 name:FVLocationManagerDidFailNotification
                                               object:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _activityIndicatorView.center = self.view.center;
    _contentView.frame = self.view.bounds;
    _listView.frame = _mapView.frame = _contentView.bounds;
}

#pragma mark - Actions

- (void)actionSwitchView {
    _listView.hidden ? [self showList] : [self showMap];
}

- (void)reloadModelAtLocation:(CLLocation *)location {
    FVFoursquareModel *model = [FVFoursquareModel new];
    [model loadVenuesAtLocation:location.coordinate success:^(NSArray *objects) {
        _listView.venues = objects;
        _mapView.venues = objects;
        
        _contentView.hidden = NO;
        [_activityIndicatorView stopAnimating];
    } failure:^(NSError *error) {
        _contentView.hidden = NO;
        [_activityIndicatorView stopAnimating];
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }];
}

#pragma mark - Observe methods

- (void)didReceiveUpdateLocationsNotification:(NSNotification *)notification {
    [self reloadModelAtLocation:notification.object];
}

- (void)didReceiveLocationManagerFailNotification:(NSNotification *)notification {
    NSError *error = notification.object;
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
}

#pragma mark - Private

- (void)showList {
    _listView.hidden = NO;
    _mapView.hidden = YES;
    [_switchViewButton setBackgroundImage:[UIImage imageNamed:@"ic_map"] forState:UIControlStateNormal];
}

- (void)showMap {
    _listView.hidden = YES;
    _mapView.hidden = NO;
    [_switchViewButton setBackgroundImage:[UIImage imageNamed:@"ic_list"] forState:UIControlStateNormal];
}

#pragma mark - FSVVenuesViewDelegate

- (void)didSelectVenue:(FVVenue *)venue {
    FVVenueDetailViewController *controller = [[FVVenueDetailViewController alloc] initWithVenue:venue];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
