//
//  FVVenueDetailViewController.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVViewController.h"

@class FVVenue;

@interface FVVenueDetailViewController : FVViewController

- (id)initWithVenue:(FVVenue *)venue; //designated initializer

@end
