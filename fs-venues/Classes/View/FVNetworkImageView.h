//
//  FVNetworkImageView.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface FVNetworkImageView : UIImageView

//methods
- (void)setImageWithURL:(NSURL *)url;

@end
