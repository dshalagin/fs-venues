//
//  FVVenuesListView.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenuesListView.h"
#import "FVVenueTableCell.h"

//------------------------------------------------------------
@interface FVVenuesListView () <UITableViewDataSource, UITableViewDelegate>
@end


//------------------------------------------------------------
@implementation FVVenuesListView {
    UITableView *_tableView;
}


#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _tableView = [[UITableView alloc] initWithFrame:frame];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [self addSubview:_tableView];
    }
    
    return self;
}

#pragma mark - View lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _tableView.frame = self.bounds;
}

#pragma mark - FVVenuesView

- (void)reloadData {
    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.venues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"VenueItemCell";
    FVVenueTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[FVVenueTableCell alloc] initWithReuseIdentifier:CellIdentifier];
    }
    
    cell.object = self.venues[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [FVVenueTableCell calculateHeightForWidth:_tableView.width withObject:self.venues[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //notify delegate
    [self.delegate didSelectVenue:self.venues[indexPath.row]];
    
    //remove cell selection
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
