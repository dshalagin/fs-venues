//
//  FVVenuesView.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@class FVVenue;
@protocol FVVenuesViewDelegate;

//------------------------------------------------------------
@interface FVVenuesView : UIView

@property (nonatomic, weak) id<FVVenuesViewDelegate> delegate;
@property (nonatomic, strong) NSArray *venues;

//methods
- (void)reloadData;

@end

//------------------------------------------------------------
@protocol FVVenuesViewDelegate <NSObject>

- (void)didSelectVenue:(FVVenue *)venue;

@end