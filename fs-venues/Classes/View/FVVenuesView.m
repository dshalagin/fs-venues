//
//  FVVenuesView.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenuesView.h"

#define kVenuesKeyPath @"venues"

@implementation FVVenuesView

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addObserver:self forKeyPath:kVenuesKeyPath options:0 context:nil];
    }
    
    return self;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:kVenuesKeyPath]) {
        [self reloadData]; //reload data on property change
    }
}

#pragma mark - Public

- (void)reloadData {
    //should be implemented in child classes
}

@end
