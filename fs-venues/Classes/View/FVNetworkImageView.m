//
//  FVNetworkImageView.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVNetworkImageView.h"

@implementation FVNetworkImageView {
    UIActivityIndicatorView *_activityIndicator;
}

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentMode = UIViewContentModeScaleAspectFit;
        
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self addSubview:_activityIndicator];
    }
    
    return self;
}

#pragma mark - UIView

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    _activityIndicator.center = CGPointMake(self.width / 2, self.height / 2);
}

#pragma mark - Public methods

- (void)setImageWithURL:(NSURL *)url {
    if (!url) { //if no url - show placeholder
        [self setPlaceholderImage];
        return;
    }
    
    [_activityIndicator startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block NSData *imageData;
        imageData = [NSData dataWithContentsOfURL:url];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            self.alpha = 0;
            [_activityIndicator stopAnimating];
            
            self.image = [UIImage imageWithData:imageData];
            [UIView animateWithDuration:0.5 animations:^{
                self.alpha = 1;
            }];
        });
    });
}

#pragma mark - Private

- (void)setPlaceholderImage {
    self.image = [UIImage imageNamed:@"ic_placeholder"];
}

@end
