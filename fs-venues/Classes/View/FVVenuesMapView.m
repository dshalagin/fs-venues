//
//  FVVenuesMapView.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenuesMapView.h"
#import "FVVenue.h"

#import <MapKit/MapKit.h>


#pragma mark - MapViewAnnotation

//----------------------------------------------------------------------
@interface MapViewAnnotation : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@property (nonatomic, strong) FVVenue *venue;

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle coordinate:(CLLocationCoordinate2D)coordinate;

@end

@implementation MapViewAnnotation

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle coordinate:(CLLocationCoordinate2D)coordinate {
    self = [super init];
    if (self) {
        _title = title;
        _subtitle = subtitle;
        _coordinate = coordinate;
    }
    
    return self;
}

@end

#pragma mark - FSVVenuesMapView

//----------------------------------------------------------------------
@interface FVVenuesMapView () <MKMapViewDelegate>
@end

//----------------------------------------------------------------------
@implementation FVVenuesMapView {
    MKMapView *_mapView;
}

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _mapView = [[MKMapView alloc] initWithFrame:frame];
        _mapView.showsUserLocation = YES;
        _mapView.delegate = self;
        [self addSubview:_mapView];
    }
    
    return self;
}


#pragma mark - View lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _mapView.frame = self.bounds;
}

#pragma mark - FVVenuesView

- (void)reloadData {
    //clear annotations
    [_mapView removeAnnotations:_mapView.annotations];
    
    //create annotations
    NSMutableArray *annotations = [NSMutableArray array];
    for (FVVenue *venue in self.venues) {
        FVVenueLocation *location = venue.location;
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [location.lat doubleValue];
        coordinate.longitude = [location.lng doubleValue];
        
        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:venue.name subtitle:nil coordinate:coordinate];
        annotation.venue = venue;
        [annotations addObject:annotation];
    }
    [_mapView addAnnotations:annotations];
    
    [self zoomToFitMapAnnotations];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *PinIdentifier = @"PinIdentifier";
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:PinIdentifier];
    if (pinView == nil) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:PinIdentifier];
        pinView.image = [UIImage imageNamed:@"ic_pin"];
    } else {
        pinView.annotation = annotation;
    }
    
    return pinView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    [self.delegate didSelectVenue:[(MapViewAnnotation *)view.annotation venue]];
}

#pragma mark - Private

-(void)zoomToFitMapAnnotations {
    if([_mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(MapViewAnnotation *annotation in _mapView.annotations) {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.2;
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.2;
    
    region = [_mapView regionThatFits:region];
    [_mapView setRegion:region animated:NO];
}

@end
