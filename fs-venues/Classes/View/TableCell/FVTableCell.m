//
//  FVTableCell.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVTableCell.h"

@implementation FVTableCell

#pragma mark - Layout methods

+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object {
    return 44.0f;
}

#pragma mark - Initialization

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

@end
