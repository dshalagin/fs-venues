//
//  FVTableCell.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface FVTableCell : UITableViewCell

@property (nonatomic, strong) id object;

//layout methods
+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object;

//initializers
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier; //designated initializer

@end
