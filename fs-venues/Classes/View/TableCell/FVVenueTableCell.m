//
//  FVVenueTableCell.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenueTableCell.h"
#import "FVVenue.h"

#define kVerticalOffset 10.0f
#define kHorizontalOffset 20.0f
#define kDistanceWidth 60.0f

@implementation FVVenueTableCell {
    UILabel *_titleLabel;
    UILabel *_categoryLabel;
    UILabel *_distanceLabel;
}

#pragma mark - Layout methods

+ (CGFloat)calculateHeightForWidth:(CGFloat)width withObject:(id)object {
    assert([object isKindOfClass:[FVVenue class]]);
    
    FVVenue *venue = (FVVenue *)object;
    
    //calculate title height
    UILabel *titleLabel = [[self class] createTitleLabel];
    titleLabel.frame = CGRectMake(kHorizontalOffset, 0, width - kDistanceWidth - (2 * kHorizontalOffset), 0);
    titleLabel.text = venue.name;
    [titleLabel sizeToFit];
    
    //calculate vategory height
    UILabel *categoryLabel = [[self class] createCategoryLabel];
    categoryLabel.frame = CGRectMake(kHorizontalOffset, 0, width - kDistanceWidth - (2 * kHorizontalOffset), 0);
    categoryLabel.text = [[venue primaryCategory] name];
    [categoryLabel sizeToFit];
    
    return kVerticalOffset + titleLabel.height + categoryLabel.height + kVerticalOffset;
}

#pragma mark - View builders

+ (UILabel *)createTitleLabel {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [FVAppStyle lightBlueColor];
    label.font = [UIFont systemFontOfSize:18.0f];
    label.highlightedTextColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    
    return label;
}

+ (UILabel *)createCategoryLabel {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [FVAppStyle lightGrayColor];
    label.font = [UIFont systemFontOfSize:14.0f];
    label.highlightedTextColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    
    return label;
}

+ (UILabel *)createDistanceLabel {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [FVAppStyle grayColor];
    label.font = [UIFont systemFontOfSize:16.0f];
    label.textAlignment = NSTextAlignmentRight;
    label.highlightedTextColor = [UIColor whiteColor];
    
    return label;
}

#pragma mark - Initialization

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        //set highlight background color
        UIView *backgroundColorView = [[UIView alloc] init];
        backgroundColorView.backgroundColor = [FVAppStyle lightBlueColor];
        [self setSelectedBackgroundView:backgroundColorView];
        
        //venue title
        _titleLabel = [[self class] createTitleLabel];
        [self.contentView addSubview:_titleLabel];
        
        //venue category
        _categoryLabel = [[self class] createCategoryLabel];
        [self.contentView addSubview:_categoryLabel];
        
        //venue distance
        _distanceLabel = [[self class] createDistanceLabel];
        [self.contentView addSubview:_distanceLabel];
    }
    
    return self;
}

#pragma mark - View lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //layout title
    _titleLabel.frame = CGRectMake(kHorizontalOffset,
                                   kVerticalOffset,
                                   self.contentView.width - kDistanceWidth - (2 * kHorizontalOffset),
                                   0);
    [_titleLabel sizeToFit];
    
    //layout category
    _categoryLabel.frame = CGRectMake(kHorizontalOffset,
                                      _titleLabel.bottom,
                                      self.contentView.width - kDistanceWidth - (2 * kHorizontalOffset),
                                      0);
    [_categoryLabel sizeToFit];
    
    //layout distance
    _distanceLabel.frame = CGRectMake(self.contentView.width - kDistanceWidth - kHorizontalOffset,
                                      0,
                                      kDistanceWidth,
                                      self.height);
}

- (void)setObject:(id)object {
    assert([object isKindOfClass:[FVVenue class]]);
    
    FVVenue *venue = (FVVenue *)object;
    
    _titleLabel.text = venue.name;
    _categoryLabel.text = [[venue primaryCategory] name];
    _distanceLabel.text = [NSString stringWithFormat:@"%@%@", venue.location.distance, NSLocalizedString(@"m", nil)];
}

@end