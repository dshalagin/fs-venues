//
//  FVAppStyle.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface FVAppStyle : NSObject

//colors
+ (UIColor *)blueColor;
+ (UIColor *)lightBlueColor;
+ (UIColor *)grayColor;
+ (UIColor *)darkGrayColor;
+ (UIColor *)lightGrayColor;

@end
