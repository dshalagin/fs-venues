//
//  FVAppConstants.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface FVAppConstants : NSObject

//Notifications
FOUNDATION_EXPORT NSString *const FVLocationManagerDidUpdateLocationsNotification;
FOUNDATION_EXPORT NSString *const FVLocationManagerDidFailNotification;

//Foursquare
FOUNDATION_EXPORT NSString *const FVFoursquareClientId;
FOUNDATION_EXPORT NSString *const FVFoursquareClientSecret;
FOUNDATION_EXPORT NSString *const FVFoursquareAPIURL;
FOUNDATION_EXPORT NSString *const FVFoursquareAPIVersion;
FOUNDATION_EXPORT NSInteger const FVFoursquareExploreRadius;

@end
