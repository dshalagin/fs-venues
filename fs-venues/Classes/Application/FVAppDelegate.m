//
//  FVAppDelegate.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVAppDelegate.h"
#import "FVVenueListViewController.h"

#import <CoreLocation/CoreLocation.h>

#define kLocationDistanceFilterInMeters 100

//------------------------------------------------------------
@interface FVAppDelegate () <CLLocationManagerDelegate>
@end


//------------------------------------------------------------
@implementation FVAppDelegate {
    CLLocationManager *_locationManager;
    BOOL _hasLocation; //state if location was captured at least once
}

#pragma mark - Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //apply custom ui settings
    [self customizeAppearence];
    
    //create window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    //create root view controller
    UIViewController *rootController = [FVVenueListViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootController];
    [self.window setRootViewController:navigationController];
    
    //show window
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self startLocationManager];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self stopLocationManager];
}

#pragma mark - Customization

- (void)customizeAppearence {
    //set status bar style
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //customize nav bar
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -CGFLOAT_MAX)  //remove title
                                                         forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBarTintColor:[FVAppStyle blueColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
}

#pragma mark - Core Location

- (void)startLocationManager {
    _locationManager = [[CLLocationManager alloc] init];
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    _locationManager.distanceFilter = kLocationDistanceFilterInMeters;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
}

- (void)stopLocationManager {
    [_locationManager stopUpdatingLocation];
    _locationManager.delegate = nil;
    _locationManager = nil;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = [locations lastObject];
    if (newLocation.horizontalAccuracy < 0) {
        return;
    }
    
    NSTimeInterval interval = [newLocation.timestamp timeIntervalSinceNow] * 1000;
    if (!_hasLocation || abs(interval) < 30) { //process only locations which were captured less than 30 sec
        _hasLocation = YES;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:FVLocationManagerDidUpdateLocationsNotification
                                                            object:newLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [[NSNotificationCenter defaultCenter] postNotificationName:FVLocationManagerDidFailNotification object:error];
    
    //stop updating
    [self stopLocationManager];
}

@end
