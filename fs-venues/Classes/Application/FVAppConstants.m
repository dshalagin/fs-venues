//
//  FVAppConstants.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVAppConstants.h"

@implementation FVAppConstants

//Notifications
NSString *const FVLocationManagerDidUpdateLocationsNotification = @"FVLocationManagerDidUpdateLocationsNotification";
NSString *const FVLocationManagerDidFailNotification = @"FVLocationManagerDidFailNotification";

//Foursquare
NSString *const FVFoursquareClientId = @"LWJA321BWD3IUMSSPNCY2YV3CK5CA0ES42B4NP1VFSMDZGCH";
NSString *const FVFoursquareClientSecret = @"AQ5CCYJEKWTRC4P1ZKEC2QCD23WQNZ3X5EXEWFPWYIWN5AVV";
NSString *const FVFoursquareAPIURL = @"http://api.foursquare.com/v2";
NSString *const FVFoursquareAPIVersion = @"20140806";
NSInteger const FVFoursquareExploreRadius = 500;

@end
