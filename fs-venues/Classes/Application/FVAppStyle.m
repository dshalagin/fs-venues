//
//  FVAppStyle.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVAppStyle.h"

@implementation FVAppStyle

#pragma mark - Colors

+ (UIColor *)blueColor {
    return [UIColor colorWithHex:@"#091D90"];
}

+ (UIColor *)lightBlueColor {
    return [UIColor colorWithHex:@"#2241DB"];
}

+ (UIColor *)grayColor {
    return [UIColor colorWithHex:@"#5B5B5B"];
}

+ (UIColor *)darkGrayColor {
    return [UIColor colorWithHex:@"#3D474B"];
}

+ (UIColor *)lightGrayColor {
    return [UIColor colorWithHex:@"#83898D"];
}

@end
