//
//  FVVenueCategory.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenueCategory.h"

@implementation FVVenueCategory

#pragma mark - Initialization

+ (id)categoryWithJSONDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithJSONDictionary:dictionary];
}

- (id)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _categoryId = dictionary[@"id"];
        _name = dictionary[@"name"];
        _primary = [dictionary[@"primary"] boolValue];
    }
    
    return self;
}

@end
