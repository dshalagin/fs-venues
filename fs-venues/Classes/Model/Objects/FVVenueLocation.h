//
//  FVVenueLocation.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface FVVenueLocation : NSObject

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lng;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSArray *addressArray;

//initialization
+ (id)locationWithJSONDictionary:(NSDictionary *)dictionary;
- (id)initWithJSONDictionary:(NSDictionary *)dictionary; //designated initializer

//methods
- (NSString *)formattedAddress;

@end
