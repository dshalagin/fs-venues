//
//  FVVenueLocation.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenueLocation.h"

@implementation FVVenueLocation

#pragma mark - Initialization

+ (id)locationWithJSONDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithJSONDictionary:dictionary];
}

- (id)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _address = dictionary[@"address"];
        _lat = dictionary[@"lat"];
        _lng = dictionary[@"lng"];
        _distance = dictionary[@"distance"];
        _addressArray = dictionary[@"formattedAddress"];
    }
    
    return self;
}

#pragma mark - Public methods

- (NSString *)formattedAddress {
    return [_addressArray componentsJoinedByString:@", "];
}


@end
