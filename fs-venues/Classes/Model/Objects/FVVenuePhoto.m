//
//  FVVenuePhoto.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenuePhoto.h"

@implementation FVVenuePhoto

#pragma mark - Initialization

+ (id)photoWithJSONDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithJSONDictionary:dictionary];
}

- (id)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _photoId = dictionary[@"id"];
        _prefix = dictionary[@"prefix"];
        _suffix = dictionary[@"suffix"];
    }
    
    return self;
}

#pragma mark - Public methods

- (NSString *)urlForSize:(CGSize)size {
    return [NSString stringWithFormat:@"%@%.0fx%.0f%@", _prefix, size.width, size.height, _suffix];
}

@end
