//
//  FVVenue.m
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenue.h"

@implementation FVVenue

#pragma mark - Initialization

+ (id)venueWithJSONDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithJSONDictionary:dictionary];
}

- (id)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _venueId = dictionary[@"id"];
        _name = dictionary[@"name"];
        
        //retrieve location
        _location = [FVVenueLocation locationWithJSONDictionary:dictionary[@"location"]];
        
        //retrieve featured photo
        NSArray *photos = dictionary[@"featuredPhotos"][@"items"];
        if ([photos count] > 0) {
            _photo = [FVVenuePhoto photoWithJSONDictionary:photos[0]];
        }
        
        //retrieve categories
        NSMutableArray *categories = [NSMutableArray new];
        for (id categoryDict in dictionary[@"categories"]) {
            FVVenueCategory *category = [FVVenueCategory categoryWithJSONDictionary:categoryDict];
            [categories addObject:category];
        }
        _categories = [NSArray arrayWithArray:categories];
    }
    
    return self;
}

#pragma mark - Public methods

- (FVVenueCategory *)primaryCategory {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"primary = 1"];
    NSArray *filteredArray = [_categories filteredArrayUsingPredicate:predicate];
    
    return [filteredArray count] > 0 ? filteredArray[0] : nil;
}


@end
