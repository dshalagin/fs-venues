//
//  FVVenue.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVVenueCategory.h"
#import "FVVenueLocation.h"
#import "FVVenuePhoto.h"

@interface FVVenue : NSObject

@property (nonatomic, strong) NSString *venueId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) FVVenueLocation *location;
@property (nonatomic, strong) FVVenuePhoto *photo;
@property (nonatomic, strong) NSArray *categories;

//initialization
+ (id)venueWithJSONDictionary:(NSDictionary *)dictionary;
- (id)initWithJSONDictionary:(NSDictionary *)dictionary; //designated initializer

//methods
- (FVVenueCategory *)primaryCategory;

@end
