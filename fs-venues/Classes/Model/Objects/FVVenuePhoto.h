//
//  FVVenuePhoto.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface FVVenuePhoto : NSObject

@property (nonatomic, strong) NSString *photoId;
@property (nonatomic, strong) NSString *prefix;
@property (nonatomic, strong) NSString *suffix;

//initialization
+ (id)photoWithJSONDictionary:(NSDictionary *)dictionary;
- (id)initWithJSONDictionary:(NSDictionary *)dictionary; //designated initializer

//methods
- (NSString *)urlForSize:(CGSize)size; //returns photo url with provided image size

@end
