//
//  FVFoursquareModel.m
//  fs-venues
//
//  Created by Denis Shalagin on 29/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import "FVFoursquareModel.h"
#import "FVVenue.h"

//------------------------------------------------------------
@interface FVFoursquareModel () <NSURLConnectionDelegate>
@end

//------------------------------------------------------------
@implementation FVFoursquareModel {
    NSOperationQueue *_queue;
}

#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
    }
    
    return self;
}

#pragma mark - Public

- (void)loadVenuesAtLocation:(CLLocationCoordinate2D)location success:(void (^)(NSArray *))success failure:(void (^)(NSError *))failure {
    //apply auth params
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:FVFoursquareClientId forKey:@"client_id"];
    [params setObject:FVFoursquareClientSecret forKey:@"client_secret"];
    [params setObject:FVFoursquareAPIVersion forKey:@"v"];
    [params setObject:@"foursquare" forKey:@"m"];
    
    //apply request params
    [params setObject:[NSString stringWithFormat:@"%i", FVFoursquareExploreRadius] forKey:@"radius"];
    [params setObject:[NSString stringWithFormat:@"%f, %f", location.latitude, location.longitude] forKey:@"ll"];
    [params setObject:@"1" forKey:@"venuePhotos"];
    [params setObject:@"1" forKey:@"sortByDistance"];
    
    //create params list
    NSMutableArray *paramsList = [NSMutableArray array];
    for (NSString *key in [params allKeys]) {
        
        NSString *value = [NSString stringWithFormat:@"%@=%@",
                           [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                           [[params valueForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [paramsList addObject:value];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@?%@", FVFoursquareAPIURL, @"/venues/explore", [paramsList componentsJoinedByString:@"&"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:url] queue:_queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError) {
            [self notifyFailure:failure withError:[connectionError localizedDescription] errorCode:[connectionError code]];
            return;
        }
        
        NSError *error;
        NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        if (error) {
            [self notifyFailure:failure withError:[error localizedDescription] errorCode:[error code]];
            return;
        }
        
        NSDictionary *resultDict = dataDict[@"response"];
        if (!resultDict) {
            [self notifyFailure:failure withError:NSLocalizedString(@"Can't parse response.", nil) errorCode:-1];
            return;
        }
        
        NSDictionary *warningDict = resultDict[@"warning"];
        if (warningDict) {
            [self notifyFailure:failure withError:warningDict[@"text"] errorCode:-1];
            return;
        }
        
        NSMutableArray *venues = [[NSMutableArray alloc] init];
        NSArray *items = [dataDict valueForKeyPath:@"response.groups.items.venue"];
        for (NSDictionary *venueDict in items[0]) {
            [venues addObject:[FVVenue venueWithJSONDictionary:venueDict]];
        }
        
        [self notifySuccess:success withObjects:[NSArray arrayWithArray:venues]];
    }];
}

#pragma mark - Private Helpers

- (void)notifyFailure:(void (^)(NSError *))failure withError:(NSString *)errorValue errorCode:(NSInteger)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
        [userInfo setObject:errorValue forKey:NSLocalizedFailureReasonErrorKey];
        failure([NSError errorWithDomain:@"FSV" code:errorCode userInfo:userInfo]);
    });
}

- (void)notifySuccess:(void (^)(NSArray *))success withObjects:(NSArray *)objects {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        success(objects);
    });
}

@end
