//
//  FVFoursquareModel.h
//  fs-venues
//
//  Created by Denis Shalagin on 29/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface FVFoursquareModel : NSObject

- (void)loadVenuesAtLocation:(CLLocationCoordinate2D)location success:(void (^)(NSArray *objects))success failure:(void (^)(NSError *error))failure;

@end
