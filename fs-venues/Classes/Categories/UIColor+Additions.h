//
//  UIColor+Additions.h
//  fs-venues
//
//  Created by Denis Shalagin on 28/12/14.
//  Copyright (c) 2014 Maxiosoftware. All rights reserved.
//

@interface UIColor (Additions)

+ (UIColor *)colorWithHex:(NSString *)hexColor;

@end
